function showError(response) {
	enableSpin();
	$('.error-message').html(response.message);
	$('.error').stop(true, false).fadeIn().delay(2000).fadeOut();
}

function showHit() {
	$('.outcome-boom').fadeIn().animate({
		bottom: '+=100px',
	}, {
		duration: 'fast', 
		queue: false,
		complete: function() {
			setTimeout(function() {
				$('.start-over').dialog({modal: true});
			}, 1000);
		}	
	});	
}

function showMadeIt() {
	$('.outcome-madeit').fadeIn().animate({
		bottom: '+=100px',
	}, {
		duration: 'slow', 
		queue: false,
		complete: function() {
			$('.outcome-madeit').delay(1000).fadeOut('fast', function() {
				enableSpin();
				$(this).css({
					bottom: '30px'
				});
			});
		}	
	});	
}

function resetGame() {
	$('.outcome-boom').css({
		display: 'none',
		bottom: '20px'
	});
	$('.selection-list-item-input:checked').prop('checked', false);
	$('label.active').removeClass('active');
	enableSpin();
}

function disableGame() {
	$('.selection-list-item-input:checked').prop('checked', false);
	$('input').prop('disabled', true);
	$('label.active').removeClass('active');
	disableSpin();
}

function enableSpin() {
	$('.spin-button').fadeTo('slow', 1).data('disabled', false);
}

function disableSpin() {
	$('.spin-button').fadeTo('slow', 0.5).data('disabled', true);
}

$(document).ready(function() {
	
	// Game lost, show play again dialog
	$('.start-over-button').click(function() {
		$('.start-over').dialog('close');
		if($(this).hasClass('yes')) {
			resetGame();
		} else {
			disableGame();
		}
	});

	// Add class proper to active selection's label
	$('input[name=number]').change(function() {
		console.log('change');
		$('label.active').removeClass('active');
		$('.selection-list-item-input:checked').siblings('.selection-list-item-label').addClass('active');

	});

	// Spin
	$('.spin-button').click(function() {
		if(!$(this).data('disabled')) {
			disableSpin();
			$.ajax({
				url: "/roulette/backend/spin.php",
				type: 'POST',
				data: {
					'number': $('.selection-list-item-input:checked').val()
				},
				dataType: 'json'
			})
			.done(function(response) {
				if(!response.requestSuccess) {
					showError(response);
					return;
				}

				if(response.hit) {
					showHit();
				} else {
					showMadeIt();
				}
			})
			.fail(function() {
				showError({
					message: 'N&aring;got gick fel.'
				});
				enableSpin();
			});
		}
	});

});