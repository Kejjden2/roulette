<?php

// Make sure we respond with json
header('Content-type: application/json');
$response = array(
	'message' => array(),
	'requestSuccess' => 0,
);

// Check the posted number
if(!array_key_exists('number', $_POST)) {
	$response['message'][] = 'Ingen siffra vald.';
	die(json_encode($response));
}

// Cast as int to avoid float number sillyness
$playerNumber = (int)$_POST['number'];
if($playerNumber < 1 || $playerNumber > 6) {
	$response['message'][] = 'Siffran &auml;r inte giltig.';
	die(json_encode($response));
}

// Generate number
$generatedNumber = rand(1,6);

if($generatedNumber == $playerNumber) {
	$response['hit'] = true;
} else {
	$response['hit'] = false;
}

// Respond
$response['requestSuccess'] = 1;
die(json_encode($response));

