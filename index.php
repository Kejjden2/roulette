<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Russian Roulette</title>

	<link href='https://fonts.googleapis.com/css?family=Passion+One:900' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Work+Sans:700' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Alfa+Slab+One' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Lato:700' rel='stylesheet' type='text/css'>
	<link href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css" rel="stylesheet" type="text/css">
	<link href="assets/css/main.css" rel="stylesheet" type="text/css">
</head>
<body>

	<div class="page">
		<h1 class="game-name">Ryska Rouletten</h1>

		<span class="spin-header">Välj en siffra:</span>
		<ul class="selection-list">
			<li class="selection-list-item">
				<input type="radio" id="selection-1" class="selection-list-item-input" value="1" name="number">
				<label for="selection-1" class="selection-list-item-label">1</label>
			</li>
			<li class="selection-list-item">
				<input type="radio" id="selection-2" class="selection-list-item-input" value="2" name="number">
				<label for="selection-2" class="selection-list-item-label">2</label>
			</li>
			<li class="selection-list-item">
				<input type="radio" id="selection-3" class="selection-list-item-input" value="3" name="number">
				<label for="selection-3" class="selection-list-item-label">3</label>
			</li>
			<li class="selection-list-item">
				<input type="radio" id="selection-4" class="selection-list-item-input" value="4" name="number">
				<label for="selection-4" class="selection-list-item-label">4</label>
			</li>
			<li class="selection-list-item">
				<input type="radio" id="selection-5" class="selection-list-item-input" value="5" name="number">
				<label for="selection-5" class="selection-list-item-label">5</label>
			</li>
			<li class="selection-list-item">
				<input type="radio" id="selection-6" class="selection-list-item-input" value="6" name="number">
				<label for="selection-6" class="selection-list-item-label">6</label>
			</li>
		</ul>
		<button class="spin-button">Snurra</button>
		
		<div class="outcome-boom">
			<span class="outcome-boom-text">BOM!</span>
		</div>

		<div class="outcome-madeit">
			<span class="outcome-madeit-text">Du klarade dig!</span>
		</div>

		<div class="error">
			<span class="error-message">Ingen siffra vald.</span>
		</div>
	</div>


	<div class="start-over">
		<span class="start-over-header">Vill du spela igen?</span>
		<button class="start-over-button no">Nej</button>
		<button class="start-over-button yes">Ja</button>
	</div>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
	<script src="assets/js/main.js"></script>
</body>
</html>